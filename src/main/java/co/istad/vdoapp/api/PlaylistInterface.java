package co.istad.vdoapp.api;

import co.istad.vdoapp.api.response.BaseResponse;
import co.istad.vdoapp.api.response.Paging;
import co.istad.vdoapp.model.Playlist;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.service.annotation.GetExchange;

import java.util.List;

public interface PlaylistInterface {

    @GetExchange("/playlists")
    BaseResponse<Paging<Playlist>> getPlaylists(@RequestParam int pageNum, @RequestParam int pageSize);

}
