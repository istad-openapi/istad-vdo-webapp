package co.istad.vdoapp.api.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Paging<T> {

    private Integer total;
    private List<T> list;
    private Integer pageNum;
    private Integer pageSize;
    private List<Integer> navigatepageNums;

}
