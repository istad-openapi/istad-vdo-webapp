package co.istad.vdoapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IstadvdoWebappApplication {

	public static void main(String[] args) {
		SpringApplication.run(IstadvdoWebappApplication.class, args);
	}

}
