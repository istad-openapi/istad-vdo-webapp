package co.istad.vdoapp.model;

import java.time.LocalDateTime;

public record Playlist(String uuid,
                       String title,
                       String ownerId,
                       String visibility,
                       String thumbnail,
                       LocalDateTime createdAt,
                       LocalDateTime updatedAt) {
}
