package co.istad.vdoapp.controller;

import co.istad.vdoapp.model.Playlist;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @GetMapping("/")
    String viewHome(Model model) {
        model.addAttribute("hasAuthor", false);
        model.addAttribute("name", "Dara");
        model.addAttribute("isLoading", false);
        return "page/index";
    }

}
