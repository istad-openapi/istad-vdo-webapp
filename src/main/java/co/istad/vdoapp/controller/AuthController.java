package co.istad.vdoapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AuthController {

    @GetMapping("login")
    String viewLogin() {
        return "page/login";
    }

    @GetMapping("register")
    String viewRegister() {
        return "page/register";
    }
}
