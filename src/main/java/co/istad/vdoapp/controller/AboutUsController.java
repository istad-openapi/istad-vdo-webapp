package co.istad.vdoapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class AboutUsController {

    @GetMapping("/aboutus")
    String viewAboutUs(Model model) {
        model.addAttribute("headingTitle", "<h1 class='page-heading'>About Us</h1>");
        return "page/aboutus";
    }

}
