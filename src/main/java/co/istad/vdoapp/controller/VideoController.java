package co.istad.vdoapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class VideoController {

    @GetMapping("/videos")
    String viewVideo() {
        return "page/video";
    }

}
