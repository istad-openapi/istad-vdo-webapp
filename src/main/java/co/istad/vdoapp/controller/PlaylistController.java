package co.istad.vdoapp.controller;

import co.istad.vdoapp.service.playlist.PlaylistServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
@RequiredArgsConstructor
public class PlaylistController {

    private final PlaylistServiceImpl playlistService;

    @GetMapping("/playlists/{uuid}")
    String viewPlaylistDetail(@PathVariable String uuid,
                              Model model) {
        // get data from service
        model.addAttribute("uuid", uuid);
        return "page/playlist-detail";
    }

    @GetMapping("/playlists")
    String viewPlaylist(Model model) {
        model.addAttribute("hasAuthor", false);
        model.addAttribute("playlists", playlistService.getPlaylistsWithPaging(1, 10).getData());
        return "page/playlist";
    }

}
