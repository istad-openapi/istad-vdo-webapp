package co.istad.vdoapp.config;

import co.istad.vdoapp.api.PlaylistInterface;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.support.WebClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;

@Component
public class WebClientConfig {

    private final String BASE_URL = "http://localhost:8080/api/v1";

    private final PlaylistInterface playlistInterface;

    public WebClientConfig() {
        WebClient webClient = WebClient.builder()
                .baseUrl(BASE_URL)
                .defaultHeader(
                        "Authorization",
                        "Basic aXQuY2hoYXlhQGdtYWlsLmNvbTpJU1RBREAyMDIz"
                )
                .build();
        HttpServiceProxyFactory httpServiceProxyFactory =
                HttpServiceProxyFactory.builder(WebClientAdapter.forClient(webClient))
                        .build();
        playlistInterface = httpServiceProxyFactory.createClient(PlaylistInterface.class);
    }

    public PlaylistInterface getPlaylistInterface() {
        return playlistInterface;
    }
}
