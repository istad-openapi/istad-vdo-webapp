package co.istad.vdoapp.service.playlist;

import co.istad.vdoapp.api.response.BaseResponse;
import co.istad.vdoapp.api.response.Paging;
import co.istad.vdoapp.model.Playlist;

import java.util.List;

public interface PlaylistService {

    BaseResponse<Paging<Playlist>> getPlaylistsWithPaging(int pageNum, int pageSize);

}
