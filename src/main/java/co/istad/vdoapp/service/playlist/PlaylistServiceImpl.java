package co.istad.vdoapp.service.playlist;

import co.istad.vdoapp.api.PlaylistInterface;
import co.istad.vdoapp.api.response.BaseResponse;
import co.istad.vdoapp.api.response.Paging;
import co.istad.vdoapp.config.WebClientConfig;
import co.istad.vdoapp.model.Playlist;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PlaylistServiceImpl implements PlaylistService {

    private final WebClientConfig webClientConfig;

    @Override
    public BaseResponse<Paging<Playlist>> getPlaylistsWithPaging(int pageNum, int pageSize) {
        return webClientConfig
                .getPlaylistInterface().getPlaylists(pageNum, pageSize);
    }
}
